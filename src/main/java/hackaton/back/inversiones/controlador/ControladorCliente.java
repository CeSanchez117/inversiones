package hackaton.back.inversiones.controlador;

import hackaton.back.inversiones.modelo.Cliente;
import hackaton.back.inversiones.servicio.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/inversiones/v1/clientes")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class ControladorCliente {

    @Autowired
    ServicioCliente servicioCliente;

    //GET
    @GetMapping
    public List<Cliente> getAllCostumers(){
        return this.servicioCliente.getAll();
    }

    //GET
    @GetMapping("/{id}")
    public Cliente getCustomerById(@PathVariable(required = false) String id){
        return this.servicioCliente.getCustomerById(id);
    }

    //POST
    @PostMapping
    public void createCostumer(@RequestBody Cliente cliente){
        cliente.setIdCliente(null);
        this.servicioCliente.addCustomer(cliente);
    }

    //UPDATE
    @PutMapping("/updateCostumer")
    public ResponseEntity updateCostumerById(@PathVariable String idCliente,
                                             @RequestBody Cliente cliente){
        return this.servicioCliente.updateCustomer(cliente);
    }

    //Delete
    @DeleteMapping("/deleteCostumer")
    public ResponseEntity deleteCostumer(@RequestBody Cliente cliente){
        return this.servicioCliente.deleteCustomer(cliente);
    }

    //GET
    @GetMapping("/getByAvgBalance")
    public List<Cliente> getProductsByRangoPrecio(@RequestParam(required = false) Double balance){
        System.out.println("Llega aca " + balance);
        if (balance == null){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }else {
            return this.servicioCliente.findByAvgBalance(balance.doubleValue());
        }
    }

}