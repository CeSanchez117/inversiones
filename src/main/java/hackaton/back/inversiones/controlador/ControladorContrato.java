package hackaton.back.inversiones.controlador;

import hackaton.back.inversiones.modelo.Cliente;
import hackaton.back.inversiones.modelo.Contrato;
import hackaton.back.inversiones.servicio.ServicioContrato;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/inversiones/v2/contratos")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class ControladorContrato {

    @Autowired
    ServicioContrato servicioContrato;

    // GET []
    @GetMapping
    public List<Contrato> obtenerProductos() {
        return this.servicioContrato.obtenerContratoTodos();
    }

    // GET {id}
    @GetMapping("/{id}")
    public Contrato obtenerProductoPorId(@PathVariable String id) {
        Contrato contrato = this.servicioContrato.buscarContratoPorId(id);
        if (contrato == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return contrato;
    }

    // POST
    @PostMapping
    public ResponseEntity guardarContrato(@RequestBody Contrato contrato) {
        contrato.setIdContrato(null);

        return this.servicioContrato.agregarContrato(contrato);
    }

    // PUT
    @PutMapping("/{id}")
    public void reemplazarContrato(@PathVariable String id,
                                   @RequestBody Contrato c) {

        final Contrato o = this.servicioContrato.buscarContratoPorId(id);
        if(o == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        this.servicioContrato.guardarContratoPorId(id, c);
    }

    //Delete
    @DeleteMapping("/{idContrato}")
    public void deleteContrato(@PathVariable String idContrato){
        this.servicioContrato.deleteContrato(idContrato);
    }

}
