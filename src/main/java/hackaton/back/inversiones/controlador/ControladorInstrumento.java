package hackaton.back.inversiones.controlador;

import hackaton.back.inversiones.modelo.Instrumento;
import hackaton.back.inversiones.servicio.ServicioInstrumento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("inversiones/v1/instrumentos")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class ControladorInstrumento {

    @Autowired
    ServicioInstrumento servicioInstrumento; // = new ServicioInstrumentoImpl();

    @GetMapping
    //EntityModel<Producto>
    public List<Instrumento> obtenerInstrumentos(@RequestParam(required = false) Double saldo){

        if(saldo != null)
            return this.servicioInstrumento.obtenerInstrumentosporSaldo(saldo.doubleValue());

        return this.servicioInstrumento.obtenerTodos();
        //this.servicioInstrumento.
    }

    @GetMapping("/{id}")
    public Instrumento obtenerInstrumentoPorId(@PathVariable String id){
        return this.servicioInstrumento.obtenerInstrumentoPorId(id);
    }

    @PostMapping
    public ResponseEntity guardaInstrumento(@RequestBody Instrumento nuevoInstrumento){
        nuevoInstrumento.setId(null);
        this.servicioInstrumento.agregarInstrumento(nuevoInstrumento);
        return new ResponseEntity<>("Instrumento creado con éxito.", HttpStatus.OK);
    }

    @DeleteMapping("/{idInstrumento}")
    public ResponseEntity eliminarProducto(@PathVariable(name = "idInstrumento") String id){
        if( this.servicioInstrumento.obtenerInstrumentoPorId(id) == null) {
            return new ResponseEntity<>("Instrumento no encontrado, imposible eliminar.", HttpStatus.NOT_FOUND);
        } else {
            this.servicioInstrumento.eliminarProducto(id);
            //"Instrumento eliminado correctamente."
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }



}
