package hackaton.back.inversiones.modelo;

import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

public class Cliente {

    @Id
    String idCliente;
    String nombreCliente;
    String apellidoPaCliente;
    String apellidoMaCliente;
    String numeroCliente;
    double saldoPromedio;
    int numeroMovimientos;
    double saldoActual;
    List<Contrato> contratosList = new ArrayList<>();

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getApellidoPaCliente() {
        return apellidoPaCliente;
    }

    public void setApellidoPaCliente(String apellidoPaCliente) {
        this.apellidoPaCliente = apellidoPaCliente;
    }

    public String getApellidoMaCliente() {
        return apellidoMaCliente;
    }

    public void setApellidoMaCliente(String apellidoMaCliente) {
        this.apellidoMaCliente = apellidoMaCliente;
    }

    public String getNumeroCliente() {
        return numeroCliente;
    }

    public void setNumeroCliente(String numeroCliente) {
        this.numeroCliente = numeroCliente;
    }

    public double getSaldoPromedio() {
        return saldoPromedio;
    }

    public void setSaldoPromedio(double saldoPromedio) {
        this.saldoPromedio = saldoPromedio;
    }

    public int getNumeroMovimientos() {
        return numeroMovimientos;
    }

    public void setNumeroMovimientos(int numeroMovimientos) {
        this.numeroMovimientos = numeroMovimientos;
    }

    public double getSaldoActual() {
        return saldoActual;
    }

    public void setSaldoActual(double saldoActual) {
        this.saldoActual = saldoActual;
    }

    public List<Contrato> getContratosList() {
        return contratosList;
    }

    public void setContratosList(List<Contrato> contratosList) {
        this.contratosList = contratosList;
    }
}