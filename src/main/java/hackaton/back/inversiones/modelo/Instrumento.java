package hackaton.back.inversiones.modelo;

import org.springframework.data.annotation.Id;

public class Instrumento {

    @Id String id;
    private String nombre;
    private double saldoMin;
    private double saldoMax;
    private int plazo;
    private double tasa;
    private String riesgo;


    public Instrumento(String id, String nombre, double saldoMin, double saldoMax, int plazo, double tasa, String riesgo) {
        this.id = id;
        this.nombre = nombre;
        this.saldoMin = saldoMin;
        this.saldoMax = saldoMax;
        this.plazo = plazo;
        this.tasa = tasa;
        this.riesgo = riesgo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getSaldoMin() {
        return saldoMin;
    }

    public void setSaldoMin(double saldoMin) {
        this.saldoMin = saldoMin;
    }

    public double getSaldoMax() {
        return saldoMax;
    }

    public void setSaldoMax(double saldoMax) {
        this.saldoMax = saldoMax;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    public double getTasa() {
        return tasa;
    }

    public void setTasa(double tasa) {
        this.tasa = tasa;
    }

    public String getRiesgo() {
        return riesgo;
    }

    public void setRiesgo(String riesgo) {
        this.riesgo = riesgo;
    }
}
