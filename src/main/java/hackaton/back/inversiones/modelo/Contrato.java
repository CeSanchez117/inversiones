package hackaton.back.inversiones.modelo;

import org.springframework.data.annotation.Id;

public class Contrato {

    @Id
    String idContrato;
    String numeroCliente;
    String idInstrumento;
    double monto;

    public String getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(String idContrato) {
        this.idContrato = idContrato;
    }

    public String getNumeroCliente() {
        return numeroCliente;
    }

    public void setNumeroCliente(String numeroCliente) {
        this.numeroCliente = numeroCliente;
    }

    public String getIdInstrumento() {
        return idInstrumento;
    }

    public void setIdInstrumento(String idInstrumento) {
        this.idInstrumento = idInstrumento;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }
}
