package hackaton.back.inversiones.servicio;

import hackaton.back.inversiones.modelo.Cliente;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ServicioCliente {

    public List<Cliente> getAll();

    public Cliente getCustomerById(String idCliente);

    public ResponseEntity addCustomer(Cliente cliente);

    public ResponseEntity updateCustomer(Cliente cliente);

    public ResponseEntity deleteCustomer(Cliente cliente);

    public List<Cliente> findByAvgBalance(Double balance);

}
