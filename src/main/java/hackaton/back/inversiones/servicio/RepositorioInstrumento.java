package hackaton.back.inversiones.servicio;

import hackaton.back.inversiones.modelo.Instrumento;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioInstrumento extends MongoRepository<Instrumento, String>,
                    RepositorioInstrumentoPersonalizado{
    //db.getCollection('instrumento').find({$and : [  {'saldoMin': {$lte: 500} }, {'saldoMax': {$gte: 500}} ]})
    @Query("{$and : [  {'saldoMin': {$lte: ?0} }, {'saldoMax': {$gte: ?0}} ]}")
    public List<Instrumento> buscarPorSaldo(double saldo);


}
