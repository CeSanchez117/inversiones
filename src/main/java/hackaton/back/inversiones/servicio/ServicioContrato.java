package hackaton.back.inversiones.servicio;

import hackaton.back.inversiones.modelo.Contrato;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ServicioContrato {
        public ResponseEntity agregarContrato(Contrato contrato);
        public Contrato buscarContratoPorId(String id);
        public List<Contrato> obtenerContratoTodos();
        public void guardarContratoPorId(String id, Contrato c);
        public void deleteContrato(String idContrato);
}
