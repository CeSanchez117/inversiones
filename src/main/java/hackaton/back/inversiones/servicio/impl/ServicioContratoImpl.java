package hackaton.back.inversiones.servicio.impl;

import hackaton.back.inversiones.modelo.Cliente;
import hackaton.back.inversiones.modelo.Contrato;
import hackaton.back.inversiones.servicio.RepositorioContrato;
import hackaton.back.inversiones.servicio.ServicioContrato;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class ServicioContratoImpl implements ServicioContrato {

    @Autowired
    RepositorioContrato repositorioContrato;

    @Override
    public ResponseEntity agregarContrato(Contrato c) {
        this.repositorioContrato.insert(c);

        //System.out.println("El contrato generado " + c.getIdContrato());
        //return this.repositorioContrato.insert(c);
        return new ResponseEntity<>(""+c.getIdContrato(), HttpStatus.OK);
    }

    @Override
    public Contrato buscarContratoPorId(String id) {
        Optional<Contrato> r = this.repositorioContrato.findById(id);
        return r.isPresent() ? r.get() : null;
    }

    @Override
    public List<Contrato> obtenerContratoTodos() {
        return this.repositorioContrato.findAll();
    }

    @Override
    public void guardarContratoPorId(String idContrato, Contrato c) {
        this.repositorioContrato.save(c);
    }

    @Override
    public void deleteContrato(String idContrato) {
        this.repositorioContrato.deleteById(idContrato);

    }

}