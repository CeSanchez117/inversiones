package hackaton.back.inversiones.servicio.impl;

import hackaton.back.inversiones.modelo.Instrumento;
import hackaton.back.inversiones.servicio.RepositorioInstrumento;
import hackaton.back.inversiones.servicio.ServicioInstrumento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioInstrumentoImpl implements ServicioInstrumento {

    @Autowired
    RepositorioInstrumento repositorioInstrumento;

    @Override
    public List<Instrumento> obtenerTodos() {
        return this.repositorioInstrumento.findAll();
    }

    @Override
    public void agregarInstrumento(Instrumento instrumento) {
        this.repositorioInstrumento.insert(instrumento);

    }

    @Override
    public Instrumento obtenerInstrumentoPorId(String id) {
        Optional<Instrumento> instrumento = this.repositorioInstrumento.findById(id);
        return instrumento.isPresent() ? instrumento.get() : null;
    }

    @Override
    public List<Instrumento> obtenerInstrumentosporSaldo(double saldo) {
        return this.repositorioInstrumento.buscarPorSaldo(saldo);
    }

    @Override
    public void eliminarProducto(String id) {
        this.repositorioInstrumento.deleteById(id);
    }
}
