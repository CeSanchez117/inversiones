package hackaton.back.inversiones.servicio.impl;

import hackaton.back.inversiones.modelo.Cliente;
import hackaton.back.inversiones.servicio.RepositorioCliente;
import hackaton.back.inversiones.servicio.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioClienteImpl implements ServicioCliente {

    @Autowired
    RepositorioCliente repositorioCliente;

    @Override
    public List<Cliente> getAll() {
        return this.repositorioCliente.findAll();
    }

    @Override
    public Cliente getCustomerById(String idCliente) {
        Optional<Cliente> r = this.repositorioCliente.findById(idCliente);
        return r.isPresent()?r.get() : null;
    }

    @Override
    public ResponseEntity addCustomer(Cliente cliente) {
        this.repositorioCliente.insert(cliente);
        return new ResponseEntity<>("Cliente guardado correctamente.", HttpStatus.OK);
    }

    @Override
    public ResponseEntity updateCustomer(Cliente cliente) {
        System.out.println("Esta entrando aca");
        Optional<Cliente> costumer = this.repositorioCliente.findById(cliente.getIdCliente());
        if (costumer.isPresent()) {
            this.repositorioCliente.save(cliente);
            return new ResponseEntity<>("Datos actualizados correctamente.", HttpStatus.OK);
        }else{
            return new ResponseEntity<>("Cliente no encontrado.", HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity deleteCustomer(Cliente cliente) {
        Optional<Cliente> costumer = this.repositorioCliente.findById(cliente.getIdCliente());

        if (costumer.isPresent()) {
        try {
            this.repositorioCliente.delete(cliente);
            return new ResponseEntity<>("Cliente eliminado correctamente.", HttpStatus.OK);
        } catch(Exception ex) {
            return new ResponseEntity<>("No se ha podido eliminar el cliente, intente más tarde.", HttpStatus.NO_CONTENT);
        }
        }else{
           return new ResponseEntity<>("Cliente no encontrado.", HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public List<Cliente> findByAvgBalance(Double balance) {
        System.out.println("En la impl "+balance);
        return this.repositorioCliente.findBysaldoPromedioRango(0.0, balance);
    }
}
