package hackaton.back.inversiones.servicio;

import hackaton.back.inversiones.modelo.Cliente;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface RepositorioCliente extends MongoRepository<Cliente, String> {

    @Query("{$and :[ {'saldoPromedio' : {$gte: ?0} }, {'saldoPromedio' : {$lte: ?1}} ]}")
    public List<Cliente> findBysaldoPromedioRango(double min, double max);
}
