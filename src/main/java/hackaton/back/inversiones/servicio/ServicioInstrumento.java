package hackaton.back.inversiones.servicio;

import hackaton.back.inversiones.modelo.Instrumento;

import java.util.List;

public interface ServicioInstrumento {

    public List<Instrumento> obtenerTodos();
    public void agregarInstrumento(Instrumento instrumento);
    public Instrumento obtenerInstrumentoPorId(String id);
    public List<Instrumento> obtenerInstrumentosporSaldo(double saldo);
    public void eliminarProducto(String id);


}
