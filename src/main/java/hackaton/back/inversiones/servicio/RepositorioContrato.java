package hackaton.back.inversiones.servicio;

import hackaton.back.inversiones.modelo.Contrato;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioContrato extends MongoRepository<Contrato, String> {
}
