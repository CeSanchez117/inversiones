package hackaton.back.inversiones.servicio;

import hackaton.back.inversiones.modelo.Instrumento;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;

import java.util.List;


public interface RepositorioInstrumentoPersonalizado {

    public List<Instrumento> buscarPorSaldoPersonalizado(double saldo);
}
